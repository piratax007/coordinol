# COORDINOL V0.0.5

## Juego de motricidad fina y coordinación mano - ojo

*Coordinol* es un juego inspirado en las pruebas psicométricas que aplican los centros de certificación para conductores nuevos en Colombia. Consiste de dos juegos en uno

### Materiales
- Una placa Arduino UNO
- Dos módulos de Joystick para Arduino
- Jumpers

### Karts
<img src="./images/karts.png" width="300"/>

Presenta dos pistas de diferentes trazado que se desplazan mientras el jugador intenta mantener cada uno de los "_karts_" dentro de las pistas. Al tener cada pista un trazado diferente, obliga a la coordinación mano ojo de forma independiente para cada mano. A medida que el "_kart_" salga de alguna de las pistas perderá puntos hasta quedar en cero el contador de puntos.

Utiliza la clase `street` para dibujar elipses de tamaño 2 px a lo largo de la altura (`height`) del lienzo. Cada elipse se desplaza sobre una curva trigonométrica de tal manera que al desplazarse $n$ elipses a la vez se obtiene la apariencia de desplazamiento de la carretera. El modelo trigonométrico empleado puede ser más o menos complejo para elevar el nivel de dificultad del juego.

### Semáforo
<img src="./images/semaforo.png" width="300"/>

Cada joystick cuenta con un pulsador asignado a un color (Rojo -> derecha, Verde -> izquierda), mientras aparecen los nombres de tres colores en pantalla (Verde, Amarillo y Rojo) escritos en diferentes color, el jugador debe accionar el pulsador correspondiente al color con el que esta escrita la palabra no el que se lea en la palabra.

#### Historial de versiones
V0.0.0 Desarrolladas las pistas, controlado mediante potenciometros.

V0.0.1 Mejorado el sketch y la clase _street_, se ha agregado la clase _kar_.

V0.0.3 Agregado el sistema de puntuación y terminación del juego _Karts_.

V0.0.4 Agregada bonificación, se activó el eje y de los joystick para que si alcanza el punto arriba del _Kart_ recupere un punto.

V0.0.5 Agregada primera versión del juego _Semáforo_.
