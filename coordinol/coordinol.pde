/************************************************************
Coordinol
Mg. Fausto M. Lagos Suárez
@piratax007 - 2018
GNU - GPLV3+
V0.0.3
************************************************************/

import processing.serial.*;
import cc.arduino.*;
Arduino arduino = new Arduino(this, Arduino.list()[0], 57600);

street[] left_border_1;
street[] right_border_1;
street[] left_border_2;
street[] right_border_2;

kart leftCar;
kart rightCar;

float Amplitude;
float widthStreet;
int negativePoints_leftKart = 0;
int negativePoints_rightKart = 0;
int points_gKarts = 27;

void setup() {
  size(500, 700);
  Amplitude = 35;
  widthStreet = 50;

  left_border_1 = new street[height];
  right_border_1 = new street[height];
  left_border_2 = new street[height];
  right_border_2 = new street[height];

  for (int i = 0; i < left_border_1.length; i++) {
    left_border_1[i] = new street(i * 0.01, i, 3.5, 0, width / 4 - widthStreet / 2);
    right_border_1[i] = new street(i * 0.01, i, 3.5, 0, width / 4 + widthStreet / 2);
    left_border_2[i] = new street(i * 0.01, i, 3, 0, 3 * width / 4 - widthStreet / 2);
    right_border_2[i] = new street(i * 0.01, i, 3, 0, 3 * width / 4 + widthStreet / 2);
  }

  // Los pulsadores de los joystick
  arduino.pinMode(52, Arduino.INPUT);
  arduino.pinMode(53, Arduino.INPUT);

  leftCar = new kart(height / 2);
  rightCar = new kart(height / 2);
}

void draw() {
  background(#A6BCD8);

  if (points_gKarts > 0){
    game_Karts();
  }else{
    lose_Screen();
  }
}

void game_Karts() {
  for (int i = 0; i < left_border_1.length; i++) {
    left_border_1[i].display(Amplitude);
    right_border_1[i].display(Amplitude);
    left_border_2[i].display(Amplitude);
    right_border_2[i].display(Amplitude);
  }

  negativePoints_leftKart = leftCar.display(left_border_1[height / 2], right_border_1[height / 2]);
  leftCar.move(arduino.analogRead(0), 0, width / 2);

  negativePoints_rightKart = rightCar.display(left_border_2[height / 2], right_border_2[height / 2]);
  rightCar.move(arduino.analogRead(3), width / 2, width);

  bonus_gCars();
}

void bonus_gCars() {
  textAlign(CENTER);
  textSize(36);
  text(points_gKarts, width / 2, 100);
  points_gKarts -= negativePoints_leftKart + negativePoints_rightKart;
}

void lose_Screen() {
  background(75);
  fill(255, 0, 0);
  textAlign(CENTER);
  textSize(64);
  text("You Lose!!!", width / 2, height / 2);
}