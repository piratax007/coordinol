/************************************************************
Clase kart
Mg. Fausto M. Lagos Suárez
@piratax007 - 2018
GNU - GPLV3+
V1.0.0
************************************************************/

class kart {
  float x, y;
  int timeCount = 0;

  kart(float yInit) {
    y = yInit;
  }

  int display(street dotLeft, street dotRight) {
    int points = 0;
    rectMode(CENTER);
    if (x < dotLeft.k  || x > dotRight.k) {
      fill(#FA5305);
      timeCount++;
      if (timeCount == 1) {
        points = 1;
      }else{
        points = 0;
      }
    } else {
      fill(#259004);
      timeCount = 0;
    }
    rect(x, y, 5, 7);
    return points;
  }

  void move(float joystickX, float mapX, float mapY) {
    x = map(joystickX, 0, 1024, mapX, mapY);
  }
}