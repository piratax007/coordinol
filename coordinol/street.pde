/************************************************************
Clase street
Mg. Fausto M. Lagos Suárez
@piratax007 - 2018
GNU - GPLV3+
V1.0.0
************************************************************/

class street{
  float x, y, A, B, C, D, k;
  
  street(float X, float Y, float T, float dh, float dv){
    C = dh;
    B = T;
    D = dv;
    x = X;
    y = Y;
  }
  
  void display(float A){
    noStroke();
    fill(52, 52, 179);
    k = A * sin(B * (x += .003) + C) + D + 25 * cos(x) + 15;
    ellipse(k, y, 2, 2);
  }
}